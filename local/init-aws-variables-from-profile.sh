#!/bin/sh

echo "WARNING: $0 should be sourced, otherwise AWS_* variables won't be defined in your environment."
PROFILE="${1:-default}"
export AWS_ACCESS_KEY_ID=$(aws configure get aws_access_key_id --profile "$PROFILE")
export AWS_SECRET_ACCESS_KEY=$(aws configure get aws_secret_access_key --profile "$PROFILE")
export AWS_DEFAULT_REGION=$(aws configure get region --profile "$PROFILE")
