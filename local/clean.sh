#!/bin/sh

set -x

local/terraform.sh apply -destroy -auto-approve
rm -rf .build
