resource "aws_ecs_service" "review_app_service" {
  name            = var.review_app_name
  cluster         = aws_ecs_cluster.review_app_cluster.id
  task_definition = aws_ecs_task_definition.review_apps_task_definition.arn
  desired_count   = 1
  launch_type     = "FARGATE"

  network_configuration {
    assign_public_ip = false

    security_groups = [
      aws_security_group.egress-all.id,
      aws_security_group.api-ingress.id,
    ]
    subnets = [
      aws_subnet.private.id,
    ]
  }
}
