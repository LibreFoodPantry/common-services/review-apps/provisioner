resource "aws_ecs_task_definition" "review_apps_task_definition" {
    family = var.review_app_name
    cpu = 256
    memory = 512
    network_mode = "awsvpc"
    requires_compatibilities = ["EC2","FARGATE"]
    execution_role_arn       = "arn:aws:iam::369104687307:role/ecsTaskExecutionRole"
    container_definitions = <<EOF
  [
    {
      "name": "review-apps-container",
      "image": "review-apps/review_apps_task_definition:latest",
      "essential" : true,
      "portMappings": [
        {
          "containerPort": 80
        }
      ],
      "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
          "awslogs-region": "us-east-2",
          "awslogs-group": "/ecs/${var.review_app_name}",
          "awslogs-stream-prefix": "ecs"
        }
      }
    }
  ]
  EOF   
}

resource "aws_cloudwatch_log_group" "review-apps-log" {
  name = "/ecs/${var.review_app_name}"
}