resource "aws_ecs_cluster" "review_app_cluster" {
  name = var.review_app_name
  capacity_providers = [
      "FARGATE"
  ]
}
