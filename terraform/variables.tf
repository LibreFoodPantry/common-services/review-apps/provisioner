variable "review_app_name" {
  type = string
  description = "The unique name of the review app for which resources will be provisioned."
}
