# If you modify this file,
# be sure to modify terraform/environments/gitlab/terraform.tf too.

terraform {
  # Use default local backend.

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}
