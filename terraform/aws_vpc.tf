resource "aws_vpc" "review-apps-vpc" {
  cidr_block = "10.0.0.0/16"

  tags = {
      Name = var.review_app_name
  }
}
