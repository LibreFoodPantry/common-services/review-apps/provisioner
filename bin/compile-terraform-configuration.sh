#!/bin/sh

ENVIRONMENT="${ENVIRONMENT:-gitlab}"

set -x

rm -v .build/*.tf*
mkdir -pv .build/
cp -v terraform/*.tf* .build/
cp -v "terraform/environments/${ENVIRONMENT}/"*.tf* .build/
