# Provisioner

Provisions AWS resources for Review Apps.

## Developers

### 1. Establish AWS credentials

Create an access key for your AWS account (see https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_access-keys.html).

Then define environment variables with your AWS credentials. See below.

#### In a Linux shell

```bash
export AWS_ACCESS_KEY_ID="anaccesskey"
export AWS_SECRET_ACCESS_KEY="asecretkey"
export AWS_DEFAULT_REGION="us-east-2"
```

> Tip:
>
> For linux, if you already use AWS CLI and have defined profiles in ~/.aws/,
> the following will define the AWS_* environment variables you need.
>
>     . ./local/init-aws-variables-from-profile.sh [profile]


#### In Windows PowerShell

```ps1
$Env:AWS_ACCESS_KEY_ID="anaccesskey"
$Env:AWS_SECRET_ACCESS_KEY="asecretkey"
$Env:AWS_DEFAULT_REGION="us-east-2"
```

### 2. Start the local development container

When developing we want to run in the same environment that GitLab CI will use.
So we'll start a Docker container built from the same image that GitLab CI will use.
Then we'll run all our commands from within this contianer. Let's start the
development container now.

#### In a Linux shell

```bash
./local/env.sh
```

#### In Windows PowerShell

Adjust its execution policy so that it can run unsigned scripts
(see https://superuser.com/questions/106360/how-to-enable-execution-of-powershell-scripts).

Then run...

```ps1
.\local\env.ps1
```

### 3. Development cycle

Edit files in `terraform/` using your favorite editor. Then, inside the local
development container, run the following commands.

```bash
./local/build.sh
./local/test.sh
./local/run.sh     # Provisions resources on AWS.
```

### 4. Cleanup

When you are done developing, delete all provisioned resources on AWS and
remove genereted files as follows.

```bash
./local/clean.sh
```
